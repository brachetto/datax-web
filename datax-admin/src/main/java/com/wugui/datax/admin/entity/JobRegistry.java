package com.wugui.datax.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * Created by xuxueli on 16/9/30.
 */
@Data
public class JobRegistry {

    private Integer id;
    private String registryGroup;
    private String registryKey;
    private String registryValue;
    private Double cpuUsage;
    private Double memoryUsage;
    private Double loadAverage;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
}
