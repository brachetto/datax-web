package com.wugui.datax.admin.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author xiang
 * @description TODO
 * @createTime 2024-06-29 下午2:25
 * @updateTime
 */
@Data
@Accessors(chain = true)
public class RestResult<T> implements Serializable {

    /**
     * 业务错误码
     */
    private long code;
    /**
     * 结果集
     */
    private T data;
    /**
     * 描述
     */
    private String msg;

    public RestResult() {
        // to do nothing
    }

    public static <T> RestResult<T> ok(T data) {
        long code = 0;
        String msg = "操作成功";
        if (data instanceof Boolean && Boolean.FALSE.equals(data)) {
            code = -1;
            msg = "操作失败";
        }
        return restResult(data, code, msg);
    }

    public static <T> RestResult<T> failed(String msg) {
        return restResult(null, -1, msg);
    }

    public static <T> RestResult<T> failed(long code, String msg) {
        return restResult(null, code, msg);
    }

    private static <T> RestResult<T> restResult(T data, long code, String msg) {
        RestResult<T> apiResult = new RestResult<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public boolean ok() {
        return 0 == code;
    }
}
