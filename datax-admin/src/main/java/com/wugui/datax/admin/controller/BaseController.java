package com.wugui.datax.admin.controller;

import com.wugui.datax.admin.dto.RestResult;
import com.wugui.datax.admin.util.JwtTokenUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

import static com.wugui.datatx.core.util.Constants.STRING_BLANK;

/**
 * base controller
 */
public class BaseController {

    /**
     * 请求成功
     *
     * @param data 数据内容
     * @param <T>  对象泛型
     * @return ignore
     */
    protected <T> RestResult<T> success(T data) {
        return RestResult.ok(data);
    }

    /**
     * 请求失败
     *
     * @param msg 提示内容
     * @return ignore
     */
    protected <T> RestResult<T> failed(String msg) {
        return RestResult.failed(msg);
    }

    /**
     * 请求失败
     *
     * @param errorCode 请求错误码
     * @return ignore
     */
    protected <T> RestResult<T> failed(long errorCode, String msg) {
        return RestResult.failed(errorCode, msg);
    }

    public Integer getCurrentUserId(HttpServletRequest request) {
        Enumeration<String> auth = request.getHeaders(JwtTokenUtils.TOKEN_HEADER);
        String token = auth.nextElement().replace(JwtTokenUtils.TOKEN_PREFIX, STRING_BLANK);
        return JwtTokenUtils.getUserId(token);
    }
}
