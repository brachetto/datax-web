SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for job_group
-- ----------------------------
DROP TABLE IF EXISTS `job_group`;
CREATE TABLE `job_group`
(
    `id`           int          NOT NULL AUTO_INCREMENT,
    `app_name`     varchar(64)  NOT NULL COMMENT '执行器AppName',
    `title`        varchar(32)  NOT NULL COMMENT '执行器名称',
    `order`        int          NOT NULL DEFAULT 0 COMMENT '排序',
    `address_type` tinyint      NOT NULL DEFAULT 0 COMMENT '执行器地址类型：0=自动注册、1=手动录入',
    `address_list` varchar(512) NULL     DEFAULT NULL COMMENT '执行器地址列表，多地址逗号分隔',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of job_group
-- ----------------------------
INSERT INTO `job_group`
VALUES (1, 'datax-executor', 'datax执行器', 1, 0, NULL);

-- ----------------------------
-- Table structure for job_info
-- ----------------------------
DROP TABLE IF EXISTS `job_info`;
CREATE TABLE `job_info`
(
    `id`                        int          NOT NULL AUTO_INCREMENT,
    `job_group`                 int          NOT NULL COMMENT '执行器主键ID',
    `job_cron`                  varchar(128) NOT NULL COMMENT '任务执行CRON',
    `job_desc`                  varchar(255) NOT NULL,
    `add_time`                  datetime     NULL     DEFAULT NULL,
    `update_time`               datetime     NULL     DEFAULT NULL,
    `author`                    varchar(64)  NULL     DEFAULT NULL COMMENT '作者',
    `alarm_email`               varchar(255) NULL     DEFAULT NULL COMMENT '报警邮件',
    `executor_route_strategy`   varchar(50)  NULL     DEFAULT NULL COMMENT '执行器路由策略',
    `executor_handler`          varchar(255) NULL     DEFAULT NULL COMMENT '执行器任务handler',
    `executor_param`            varchar(512) NULL     DEFAULT NULL COMMENT '执行器任务参数',
    `executor_block_strategy`   varchar(50)  NULL     DEFAULT NULL COMMENT '阻塞处理策略',
    `executor_timeout`          int          NOT NULL DEFAULT 0 COMMENT '任务执行超时时间，单位秒',
    `executor_fail_retry_count` int          NOT NULL DEFAULT 0 COMMENT '失败重试次数',
    `glue_type`                 varchar(50)  NOT NULL COMMENT 'GLUE类型',
    `glue_source`               mediumtext   NULL COMMENT 'GLUE源代码',
    `glue_remark`               varchar(128) NULL     DEFAULT NULL COMMENT 'GLUE备注',
    `glue_update_time`          datetime     NULL     DEFAULT NULL COMMENT 'GLUE更新时间',
    `child_jobid`               varchar(255) NULL     DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
    `trigger_status`            tinyint      NOT NULL DEFAULT 0 COMMENT '调度状态：0-停止，1-运行',
    `trigger_last_time`         bigint       NOT NULL DEFAULT 0 COMMENT '上次调度时间',
    `trigger_next_time`         bigint       NOT NULL DEFAULT 0 COMMENT '下次调度时间',
    `job_json`                  text         NULL COMMENT 'datax运行脚本',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for job_jdbc_datasource
-- ----------------------------
DROP TABLE IF EXISTS `job_jdbc_datasource`;
CREATE TABLE `job_jdbc_datasource`
(
    `id`                bigint        NOT NULL AUTO_INCREMENT COMMENT '自增主键',
    `datasource_name`   varchar(200)  NOT NULL COMMENT '数据源名称',
    `datasource_group`  varchar(200)  NULL     DEFAULT 'Default' COMMENT '数据源分组',
    `jdbc_username`     varchar(100)  NOT NULL COMMENT '用户名',
    `jdbc_password`     varchar(200)  NOT NULL COMMENT '密码',
    `jdbc_url`          varchar(500)  NOT NULL COMMENT 'jdbc url',
    `jdbc_driver_class` varchar(200)  NULL     DEFAULT NULL COMMENT 'jdbc驱动类',
    `status`            tinyint       NOT NULL DEFAULT 1 COMMENT '状态：0删除 1启用 2禁用',
    `create_by`         varchar(20)   NULL     DEFAULT NULL COMMENT '创建人',
    `create_date`       datetime      NULL     DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
    `update_by`         varchar(20)   NULL     DEFAULT NULL COMMENT '更新人',
    `update_date`       datetime      NULL     DEFAULT NULL COMMENT '更新时间',
    `comments`          varchar(1000) NULL     DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
    COMMENT = 'jdbc数据源配置'
  ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for job_lock
-- ----------------------------
DROP TABLE IF EXISTS `job_lock`;
CREATE TABLE `job_lock`
(
    `lock_name` varchar(50) NOT NULL COMMENT '锁名称',
    PRIMARY KEY (`lock_name`) USING BTREE
) ENGINE = InnoDB
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of job_lock
-- ----------------------------
INSERT INTO `job_lock`
VALUES ('schedule_lock');

-- ----------------------------
-- Table structure for job_log
-- ----------------------------
DROP TABLE IF EXISTS `job_log`;
CREATE TABLE `job_log`
(
    `id`                        bigint       NOT NULL AUTO_INCREMENT,
    `job_group`                 int          NOT NULL COMMENT '执行器主键ID',
    `job_id`                    int          NOT NULL COMMENT '任务，主键ID',
    `job_desc`                  varchar(255) NULL     DEFAULT NULL,
    `executor_address`          varchar(255) NULL     DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
    `executor_handler`          varchar(255) NULL     DEFAULT NULL COMMENT '执行器任务handler',
    `executor_param`            varchar(512) NULL     DEFAULT NULL COMMENT '执行器任务参数',
    `executor_sharding_param`   varchar(20)  NULL     DEFAULT NULL COMMENT '执行器任务分片参数，格式如 1/2',
    `executor_fail_retry_count` int          NULL     DEFAULT 0 COMMENT '失败重试次数',
    `trigger_time`              datetime     NULL     DEFAULT NULL COMMENT '调度-时间',
    `trigger_code`              int          NOT NULL COMMENT '调度-结果',
    `trigger_msg`               text         NULL COMMENT '调度-日志',
    `handle_time`               datetime     NULL     DEFAULT NULL COMMENT '执行-时间',
    `handle_code`               int          NOT NULL COMMENT '执行-状态',
    `handle_msg`                text         NULL COMMENT '执行-日志',
    `alarm_status`              tinyint      NOT NULL DEFAULT 0 COMMENT '告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败',
    `process_id`                varchar(20)  NULL     DEFAULT NULL COMMENT 'datax进程Id',
    `max_id`                    bigint       NULL     DEFAULT NULL COMMENT '增量表max id',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_job_log_trigger_time` (`trigger_time`) USING BTREE,
    INDEX `idx_job_log_handle_code` (`handle_code`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for job_log_report
-- ----------------------------
DROP TABLE IF EXISTS `job_log_report`;
CREATE TABLE `job_log_report`
(
    `id`            int      NOT NULL AUTO_INCREMENT,
    `trigger_day`   datetime NULL     DEFAULT NULL COMMENT '调度-时间',
    `running_count` int      NOT NULL DEFAULT 0 COMMENT '运行中-日志数量',
    `suc_count`     int      NOT NULL DEFAULT 0 COMMENT '执行成功-日志数量',
    `fail_count`    int      NOT NULL DEFAULT 0 COMMENT '执行失败-日志数量',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `i_trigger_day` (`trigger_day`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of job_log_report
-- ----------------------------
INSERT INTO `job_log_report`
VALUES (0, '2019-12-07 00:00:00', 0, 0, 0);
INSERT INTO `job_log_report`
VALUES (1, '2019-12-10 00:00:00', 77, 52, 23);
INSERT INTO `job_log_report`
VALUES (2, '2019-12-11 00:00:00', 9, 2, 11);
INSERT INTO `job_log_report`
VALUES (3, '2019-12-13 00:00:00', 9, 48, 74);
INSERT INTO `job_log_report`
VALUES (4, '2019-12-12 00:00:00', 10, 8, 30);
INSERT INTO `job_log_report`
VALUES (5, '2019-12-14 00:00:00', 78, 45, 66);
INSERT INTO `job_log_report`
VALUES (6, '2019-12-15 00:00:00', 24, 76, 9);
INSERT INTO `job_log_report`
VALUES (7, '2019-12-16 00:00:00', 23, 85, 10);

-- ----------------------------
-- Table structure for job_glue
-- ----------------------------
DROP TABLE IF EXISTS `job_log_glue`;
CREATE TABLE `job_log_glue`
(
    `id`          int          NOT NULL AUTO_INCREMENT,
    `job_id`      int          NOT NULL COMMENT '任务，主键ID',
    `glue_type`   varchar(50)  NULL DEFAULT NULL COMMENT 'GLUE类型',
    `glue_source` mediumtext   NULL COMMENT 'GLUE源代码',
    `glue_remark` varchar(128) NOT NULL COMMENT 'GLUE备注',
    `add_time`    datetime     NULL DEFAULT NULL,
    `update_time` datetime     NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for job_registry
-- ----------------------------
DROP TABLE IF EXISTS `job_registry`;
CREATE TABLE `job_registry`
(
    `id`             int          NOT NULL AUTO_INCREMENT,
    `registry_group` varchar(50)  NOT NULL,
    `registry_key`   varchar(191) NOT NULL,
    `registry_value` varchar(191) NOT NULL,
    `update_time`    datetime     NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_job_registry_g_k_v` (`registry_group`, `registry_key`, `registry_value`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for job_user
-- ----------------------------
DROP TABLE IF EXISTS `job_user`;
CREATE TABLE `job_user`
(
    `id`         int          NOT NULL AUTO_INCREMENT,
    `username`   varchar(50)  NOT NULL COMMENT '账号',
    `password`   varchar(100) NOT NULL COMMENT '密码',
    `role`       varchar(50)  NULL DEFAULT NULL COMMENT '角色：0-普通用户、1-管理员',
    `permission` varchar(255) NULL DEFAULT NULL COMMENT '权限：执行器ID列表，多个逗号分割',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `udx_job_user_username` (`username`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of job_user
-- ----------------------------
INSERT INTO `job_user`
VALUES (1, 'admin', '$2a$10$2KCqRbra0Yn2TwvkZxtfLuWuUP5KyCWsljO/ci5pLD27pqR3TV1vy', 'ROLE_ADMIN', NULL);


/**
v2.1.1脚本更新
*/
ALTER TABLE `job_info`
    ADD COLUMN `replace_param` VARCHAR(100) NULL DEFAULT NULL COMMENT '动态参数' AFTER `job_json`,
    ADD COLUMN `jvm_param`     VARCHAR(200) NULL DEFAULT NULL COMMENT 'jvm参数' AFTER `replace_param`,
    ADD COLUMN `time_offset`   INT          NULL DEFAULT '0' COMMENT '时间偏移量' AFTER `jvm_param`;
/**
增量改版脚本更新
 */
ALTER TABLE `job_info`
DROP COLUMN `time_offset`;
ALTER TABLE `job_info`
    ADD COLUMN `inc_start_time` DATETIME NULL DEFAULT NULL COMMENT '增量初始时间' AFTER `jvm_param`;

-- ----------------------------
-- Table structure for job_template
-- ----------------------------
DROP TABLE IF EXISTS `job_template`;
CREATE TABLE `job_template`
(
    `id`                        int          NOT NULL AUTO_INCREMENT,
    `job_group`                 int          NOT NULL COMMENT '执行器主键ID',
    `job_cron`                  varchar(128) NOT NULL COMMENT '任务执行CRON',
    `job_desc`                  varchar(255) NOT NULL,
    `add_time`                  datetime     NULL     DEFAULT NULL,
    `update_time`               datetime     NULL     DEFAULT NULL,
    `user_id`                   int          NOT NULL COMMENT '修改用户',
    `alarm_email`               varchar(255) NULL     DEFAULT NULL COMMENT '报警邮件',
    `executor_route_strategy`   varchar(50)  NULL     DEFAULT NULL COMMENT '执行器路由策略',
    `executor_handler`          varchar(255) NULL     DEFAULT NULL COMMENT '执行器任务handler',
    `executor_param`            varchar(512) NULL     DEFAULT NULL COMMENT '执行器参数',
    `executor_block_strategy`   varchar(50)  NULL     DEFAULT NULL COMMENT '阻塞处理策略',
    `executor_timeout`          int          NOT NULL DEFAULT 0 COMMENT '任务执行超时时间，单位秒',
    `executor_fail_retry_count` int          NOT NULL DEFAULT 0 COMMENT '失败重试次数',
    `glue_type`                 varchar(50)  NOT NULL COMMENT 'GLUE类型',
    `glue_source`               mediumtext   NULL COMMENT 'GLUE源代码',
    `glue_remark`               varchar(128) NULL     DEFAULT NULL COMMENT 'GLUE备注',
    `glue_update_time`          datetime     NULL     DEFAULT NULL COMMENT 'GLUE更新时间',
    `child_jobid`               varchar(255) NULL     DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
    `trigger_last_time`         bigint       NOT NULL DEFAULT 0 COMMENT '上次调度时间',
    `trigger_next_time`         bigint       NOT NULL DEFAULT 0 COMMENT '下次调度时间',
    `job_json`                  text         NULL COMMENT 'datax运行脚本',
    `jvm_param`                 varchar(200) NULL     DEFAULT NULL COMMENT 'jvm参数',
    `project_id`                int          NULL     DEFAULT NULL COMMENT '所属项目Id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;

/**
添加数据源字段
 */
ALTER TABLE `job_jdbc_datasource`
    ADD COLUMN `datasource` VARCHAR(45) NOT NULL COMMENT '数据源' AFTER `datasource_name`;

/**
添加分区字段
 */
ALTER TABLE `job_info`
    ADD COLUMN `partition_info` VARCHAR(100) NULL DEFAULT NULL COMMENT '分区信息' AFTER `inc_start_time`;

/**
2.1.1版本新增----------------------------------------------------------------------------------------------
 */

/**
最近一次执行状态
 */
ALTER TABLE `job_info`
    ADD COLUMN `last_handle_code` INT NULL DEFAULT '0' COMMENT '最近一次执行状态' AFTER `partition_info`;

/**
zookeeper地址
 */
ALTER TABLE `job_jdbc_datasource`
    ADD COLUMN `zk_address` VARCHAR(200) NULL DEFAULT NULL AFTER `jdbc_driver_class`;

ALTER TABLE `job_info`
    CHANGE COLUMN `executor_timeout` `executor_timeout` INT NOT NULL DEFAULT '0' COMMENT '任务执行超时时间，单位分钟';

/**
用户名密码改为非必填
 */
ALTER TABLE `job_jdbc_datasource`
    CHANGE COLUMN `jdbc_username` `jdbc_username` VARCHAR(100) NULL DEFAULT NULL COMMENT '用户名',
    CHANGE COLUMN `jdbc_password` `jdbc_password` VARCHAR(200) NULL DEFAULT NULL COMMENT '密码';
/**
添加mongodb数据库名字段
 */
ALTER TABLE `job_jdbc_datasource`
    ADD COLUMN `database_name` VARCHAR(45) NULL DEFAULT NULL COMMENT '数据库名' AFTER `datasource_group`;
/**
添加执行器资源字段
 */
ALTER TABLE `job_registry`
    ADD COLUMN `cpu_usage`    DOUBLE NULL AFTER `registry_value`,
    ADD COLUMN `memory_usage` DOUBLE NULL AFTER `cpu_usage`,
    ADD COLUMN `load_average` DOUBLE NULL AFTER `memory_usage`;

-- ----------------------------
-- Table structure for job_permission
-- ----------------------------
DROP TABLE IF EXISTS `job_permission`;
CREATE TABLE `job_permission`
(
    `id`          int          NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`        varchar(50)  NOT NULL COMMENT '权限名',
    `description` varchar(11)  NULL DEFAULT NULL COMMENT '权限描述',
    `url`         varchar(255) NULL DEFAULT NULL,
    `pid`         int          NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;



ALTER TABLE `job_info`
    ADD COLUMN `replace_param_type` varchar(255) NULL COMMENT '增量时间格式' AFTER `last_handle_code`;


ALTER TABLE `job_info`
    ADD COLUMN `project_id` int NULL COMMENT '所属项目id' AFTER `job_desc`;

ALTER TABLE `job_info`
    ADD COLUMN `reader_table`   VARCHAR(255) NULL COMMENT 'reader表名称' AFTER `replace_param_type`,
    ADD COLUMN `primary_key`    VARCHAR(50)  NULL COMMENT '增量表主键' AFTER `reader_table`,
    ADD COLUMN `inc_start_id`   VARCHAR(20)  NULL COMMENT '增量初始id' AFTER `primary_key`,
    ADD COLUMN `increment_type` TINYint      NULL COMMENT '增量类型' AFTER `inc_start_id`,
    ADD COLUMN `datasource_id`  BIGINT       NULL COMMENT '数据源id' AFTER `increment_type`;

DROP TABLE IF EXISTS job_project;
CREATE TABLE `job_project`
(
    `id`          int          NOT NULL AUTO_INCREMENT COMMENT 'key',
    `name`        varchar(100) NULL DEFAULT NULL COMMENT 'project name',
    `description` varchar(200) NULL DEFAULT NULL,
    `user_id`     int          NULL DEFAULT NULL COMMENT 'creator id',
    `flag`        tinyint      NULL DEFAULT 1 COMMENT '0 not available, 1 available',
    `create_time` datetime     NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT 'create time',
    `update_time` datetime     NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT 'update time',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  ROW_FORMAT = Dynamic;


ALTER TABLE `job_info`
    CHANGE COLUMN `author` `user_id` INT NOT NULL COMMENT '修改用户';

ALTER TABLE `job_info`
    CHANGE COLUMN `increment_type` `increment_type` tinyint NULL DEFAULT 0 COMMENT '增量类型';

-- --------------------------------------------------------------------------------------------------------------------
-- 2.1.3
-- --------------------------------------------------------------------------------------------------------------------
# ALTER TABLE `job_jdbc_datasource`
#     CHANGE COLUMN `database_name` `database` VARCHAR(45) NULL DEFAULT NULL COMMENT '数据库名' ;
#
# ALTER TABLE `job_jdbc_datasource`
#     CHANGE COLUMN `jdbc_username` `user_name` VARCHAR(100) CHARACTER SET 'utf8mb4' NULL DEFAULT NULL COMMENT '用户名' ,
#     CHANGE COLUMN `jdbc_password` `password` VARCHAR(200) CHARACTER SET 'utf8mb4' NULL DEFAULT NULL COMMENT '密码' ;
#
#
# ALTER TABLE `job_jdbc_datasource`
#     DROP COLUMN `database`,
#     DROP COLUMN `zk_adress`,
#     DROP COLUMN `jdbc_driver_class`,
#     DROP COLUMN `jdbc_url`,
#     DROP COLUMN `password`,
#     DROP COLUMN `user_name`,
#     DROP COLUMN `datasource`,
#     ADD COLUMN `connection_params` TEXT NOT NULL AFTER `datasource_group`,
#     CHANGE COLUMN `comments` `comments` VARCHAR(1000) CHARACTER SET 'utf8mb4' NULL DEFAULT NULL COMMENT '备注' AFTER `status`;
#
# ALTER TABLE `job_jdbc_datasource`
#     ADD COLUMN `type` VARCHAR(45) NULL COMMENT '数据源类型' AFTER `update_date`;
# ALTER TABLE `job_jdbc_datasource`
#     CHANGE COLUMN `type` `type` VARCHAR(45) NULL DEFAULT NULL COMMENT '数据源类型' AFTER `connection_params`;
