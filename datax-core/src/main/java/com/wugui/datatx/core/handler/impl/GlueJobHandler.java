package com.wugui.datatx.core.handler.impl;

import com.wugui.datatx.core.biz.model.ReturnT;
import com.wugui.datatx.core.biz.model.TriggerParam;
import com.wugui.datatx.core.handler.IJobHandler;
import com.wugui.datatx.core.log.JobLogger;

/**
 * glue job handler
 * @author xuxueli 2016-5-19 21:05:45
 */
public class GlueJobHandler extends IJobHandler {

	private long glueUpdateTime;
	private IJobHandler jobHandler;
	public GlueJobHandler(IJobHandler jobHandler, long glueUpdateTime) {
		this.jobHandler = jobHandler;
		this.glueUpdateTime = glueUpdateTime;
	}
	public long getGlueUpdateTime() {
		return glueUpdateTime;
	}

	@Override
	public ReturnT<String> execute(TriggerParam tgParam) throws Exception {
		JobLogger.log("----------- glue.version:"+ glueUpdateTime +" -----------");
		return jobHandler.execute(tgParam);
	}
}
